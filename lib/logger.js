"use strict";

const winston = require('winston');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const logDir = 'logs'; // directory path for logs

if (!fs.existsSync(logDir)) {
    // Create the directory if it does not exist
    fs.mkdirSync(logDir);
}

const LOG_LEVELS = ['error', 'warn', 'info', 'verbose', 'debug', 'silly'];

const transportsList = [
    new(winston.transports.File)({
        name: 'error-log',
        json: false,
        prettyPrint: true,
        handleExceptions: true,
        filename: path.join(logDir, '/error.log'),
        timestamp: () => Date().toString(),
        level: 'error'
    }),
    new(winston.transports.File)({
        name: 'combined-log',
        json: false,
        prettyPrint: true,
        handleExceptions: true,
        filename: path.join(logDir, '/combined.log'),
        timestamp: () => Date().toString(),
        level: (process.env.LOG_LEVEL && _.includes(LOG_LEVELS, process.env.LOG_LEVEL)) ? process.env.LOG_LEVEL : 'info'
    }),
];

if (process.env.NODE_ENV !== 'production') {
    transportsList.push(new(winston.transports.Console)({
        name: 'console.log',
        colorize: true,
        prettyPrint: true,
        handleExceptions: true,
        level: (process.env.LOG_LEVEL  && _.includes(LOG_LEVELS, process.env.LOG_LEVEL)) ? process.env.LOG_LEVEL : 'info',
        timestamp: () => Date().toString()
    }))
}

const logger = new(winston.Logger)({
    exitOnError: false,
    transports: transportsList
});

const loggerWrapper = Object.create(logger);

// Added support for Logger.verbose
loggerWrapper.verbose = function(...args) {
    return this.debug(...args);
};

module.exports = loggerWrapper;