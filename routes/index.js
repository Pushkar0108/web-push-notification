/* jslint node: true */
'use strict';
const router = require('express').Router();
const notificationRoutes = require('./notification.routes');


router.use('/notification', notificationRoutes);


module.exports = router;
