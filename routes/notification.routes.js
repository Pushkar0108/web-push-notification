'use strict';
const Router = require('express').Router();

const error = require('../middlewares/error');
const Notification = require('../api/notification');

Router.get('/notify', function(req, res, next){
    Notification.send()
        .then(response => {
            return res.status(200).json(response);
        })
        .catch(error => {
            return next(error);
        });
}, error);

Router.get('/subscribe', function(req, res, next){
    Notification.saveToken(req.query)
        .then(response => {
            return Notification.getStore();
        })
        .then(store => {
            return res.status(200).json(store);
        })
        .catch(error => {
            return next(error);
        });
    
}, error);

module.exports = Router;