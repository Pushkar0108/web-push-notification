"use strict";

const env = process.env.NODE_ENV || 'development';

module.exports = function(err, req, res, next) {

  let code = err.status || 500;
  let response = { error: err.message || err, stack: err.stack?err.stack.split('\n') : ''};

  if (err.errorCode) {
    response.errorCode = err.errorCode;
    response.errorParam = err.errorParam;
  }

  if (err.data) {
    response.data = err.data;
  }
  if(err.url){
    response.url = err.url;
  }
  if(err.statusCode){
    response.statusCode = err.statusCode;
  }

  if(code>=500){
    console.log(err.stack);
  }
  
  if (env.toLowerCase() == 'production') {
    if (code == 500) {
      response.error = 'An unexpected error has occured';
    }
    response.stack = undefined;
  }
  res.status(code).json(response);
};
