"use strict";

const http = require('http');
const express = require('express');
// const Logger = require('./lib/logger');
const ErrorHandler = require('./middlewares/error');
const router = express.Router();

const app = express();
app.use(express.static('public'));
app.set('port', process.env.PORT || 3332);
app.use(router);

router.use(require('./routes/index'));

app.use(ErrorHandler);
app.use(function(req, res, next) {
  res.send(404);
});

http.createServer(app)
    .on('error', function(err) {
        // Logger.error(err);
        console.log('"err', err);
        process.exit(1);
    })
    .listen(app.get('port'), function() {
        console.log("Market Physical Fulfillment is listening on port: " + app.get('port') + ' in ' + app.get('env'));
        // Logger.info("Market Physical Fulfillment is listening on port: " + app.get('port') + ' in ' + app.get('env'));
    });





let init = function(){
  // makeRequest();
};



module.exports = init;

(function() {
    if (require.main === module) {
        init();
    }
}());

