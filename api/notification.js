'use strict';
const Request = require('request');
const Logger = require('../lib/logger');

class Notification {

    constructor(SERVER_KEY){
        this.SERVER_KEY = SERVER_KEY;
        this.store = {};
    }

    sendNotification(CLIENT_ACCESS_TOKEN) {
        CLIENT_ACCESS_TOKEN = "fhblJMX8xTc:APA91bEDmjm-9GPJjbJ2LiVQdWTpPxe3XghgHf0XvQ-BAqiKaVYdx-qqSvo3N_qEReRfFINgE44yQYFKv0ysUfNOayzJOYENotJ8KqYBzmxUUeuxodPz2w8jVCAUWnlH0226sB0HpYTS";
        
        let reqOpts = {
          url: "https://fcm.googleapis.com/fcm/send",
          method: "POST",
          json: true,
          headers: {
            "Authorization": "key=" + this.SERVER_KEY,
            "Content-Type": "application/json"
          },
          body: {
              "notification": {
                "title": "Portugal vs. Denmark",
                "body": "5 to 1",
                "icon": "firebase-logo.png",
                "click_action": "http://localhost:8081"
              },
              "to": CLIENT_ACCESS_TOKEN
         }
        };

        return new Promise((resolve, reject) => {
            Request(reqOpts, (error, response, body) => {
                if (error || !response || response.statusCode !== 200) {
                    error = new Error("Sending notification failed :: " + (error ? error.message : JSON.stringify(body)));
                    Logger.error("Sending notification failed: ", {
                      reqOpts: reqOpts,
                      error: error
                    });
        
                    return reject(error);
                } 
                
                return resolve(body);
            });
        });
    }

    saveToken({userId, accessToken} = {}) {
        return new Promise((resolve, reject) => {
            this.store[userId] = accessToken;
            Logger.info("Token saved: ", {
                userId: userId,
                accessToken: accessToken
            });
            
            return resolve(userId);
        });
    }

    removeToken(userId) {
        delete this.store[userId];
    }

    send(userId) {
        let promiseArr = Object.keys(this.store).map(userId => {
            return this.sendNotification(this.store[userId]);
        });

        return Promise.all(promiseArr)
            .then(response => {
                Logger.info("Sending notification success: ", response);

                return response;
            })
            .catch(error => {
                Logger.error("Sending notification failed: ", error);

                Promise.reject(error);
            });
    }

    getStore() {
        return new Promise((resolve, reject) => {
            return resolve(this.store);
        });
    }
}

module.exports = new Notification("AAAAw2EQisk:APA91bF3vEP6Xt1EpqOwNLiC10rCdO72dqksMMUxTQZofWsLCDNBcooMuHwDggM5j6UrUo1NlFzaWkTvcmFXFMxSZZhV-f0Ub5YxHkYaQExpp9XxkNtZpJQ4O8cf-A73KKEBXqhpGC8A");